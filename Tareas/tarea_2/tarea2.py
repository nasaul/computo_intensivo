import numpy as np
import time
import scipy

n_iter=25000 # iteraciones
k=0.2 # time step

a = -600.0
b = 600.0
h = 0.4
r=(k/h*h)

x = np.arange(a, b+h, h) #b+h porque recuerda numpy se come el ultimo, m, m-1 etc
n_x = len(x)

y_init = np.zeros(n_x)

y_init[0] = 1
y_init[n_x-1] = 1

y_next = y_init.copy()

a=time.time()

for i in range(n_iter):
         y_antes = y_next.copy()
         y_next[1:-1] = (1-2*r)*y_antes[1:-1] + r*(y_antes[2:] + y_antes[:-2])
         # Los extremos no se actualizan!!
         y_next[0]=1.0
         y_next[-1]=1.0

print(time.time()-a)
