import numpy as np
import time
# Pregunta 2
arr = np.random.randn(4, 4)
arr[1, :]
arr[:, 2]

arr[0:2, 0:2] = 5.4
# Pregunta 3

# Update de un Laplaciano discretizado con diferencias finitas
# u[i,j] se evalua en todos los interiores menos la frontera
# porque el update de cada punto depende de los vecnios cercanos
# i.e. i+1, i-1 etc. El grid es no unforme es decir dx neq dy

dx = 0.1
dy = 0.1
dx2 = dx*dx
dy2 = dy*dy

def unumpy(u):
    u[1:-1, 1:-1] = ((u[2:, 1:-1] + u[:-2, 1:-1]) * dy2 +
                     (u[1:-1, 2:] + u[1:-1, :-2]) * dx2) / (2 * (dx2 + dy2))

def py_puro(u):
    nx, ny = u.shape
    for i in range(1,nx-1):
        for j in range(1, ny-1):
            u[i,j] = ((u[i+1, j] + u[i-1, j]) * dy2 +
                      (u[i, j+1] + u[i, j-1]) * dx2) / (2*(dx2+dy2))

# N: tamanio del grid
# Niter: numero de iteraciones
# la funcion para el update ya sea py_puro o la unumpy


def ejecuta(N, Niter, func, args=()):
    u = np.zeros([N, N])
    u[0] = 1
    for i in range(Niter):
        func(u,*args)
        # print(i);
    return u

a=time.time()
print(ejecuta(5, 1, py_puro, args=()))
print(time.time()-a)
a=time.time()
print(ejecuta(5, 1, unumpy, args = ()))
print(time.time()-a)

unif_arr = np.random.uniform(-1, 1, [100, 100])
print('Media de una distribución uniforme -1, 1', unif_arr.mean())
print('Desviación estandar de una distribución uniformme -1, 1', unif_arr.std())
normal_arr = np.random.normal(5, 2, [100, 100])
print('Media de una distribución normal 5, 2', normal_arr.mean())
print('Error estandar de una distribución normal 5, 2', normal_arr.std())
