import scipy.integrate as int
import numpy as np
import math as m
import scipy.optimize as opt

def primer_funcion(x):
    return (1 + x**2)

def segunda_funcion(x):
    return m.exp(-x**2)

primer_integral  = int.quad(primer_funcion, 1, 3.5)
segunda_integral = int.quad(segunda_funcion, 0, np.inf)
print("La integral de la primera ecuación es\t", primer_integral[0])
print("La integral de la segunda ecuación es\t", segunda_integral[0])

def funcion_min(x):
    return (x+4)*(x+1)*(x-1)*(x-3)

minimo = opt.minimize(funcion_min, x0 = -2)
print("El mínimo de la función es:\t\t",minimo.x[0])
