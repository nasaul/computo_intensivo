# Cloud computing

El artículo habla del uso de los centros de datos (vs super computadoras) que cada vez son más fáciles de usar como AWS y que los investigadores pueden pagar por muchas computadoras y paralelizar su código en lugar de correrlo por mucho tiempo.

Las tecnologías que son buenas para estos clusters son MPI

# Challenges and Opportunities for Data-Intensive Computing in the Cloud

# Las limitaciones del cómputo en la nube

  - Transferencia de datos: los sistemas de copia de archivos como scp y http son muy lentos. Se necesitan tecnologías de transferencia con velocidades mayores.

# Los retos para aplicaciones intensivas en datos

   - Se necesita tecnología que mueva grandes cantidades de datos a la nube.
   - Se necesita tecnología que mueva los datos entre nubes.
   - La escalabilidad de movimientos de datos en la nube es muy inestable.
   - Se necesita tecnología que lidie con multiples inputs de datos.
   - 
