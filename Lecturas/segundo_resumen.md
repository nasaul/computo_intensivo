# The changing paradigm of data-intensive computing

Las definiciones comunes de cómputo intensivo en datos se concentran en el manejo de bases de datos masivos, sin embargo, las nuevas aplicaciones requieren poder procesar rápidamente estas cantidades de información.

La definición que propone el artículo es:

  - El computo intensivo en datos es el manejo, análisis y entendimiento de datos en volúmenes y tasas que empujan las fronteras de la tecnología actual.

## Estilos de cómputo intensivo en datos

### Pipelines de preprocesamiento

El primer paso es reducir el tamaño de los datos.

El segundo paso es aplicar algoritmos que crean información que sea más digerible para otros procesos.

El tercer paso es usar los resultados del análisis anterior para que los usuarios puedan digerir la información y tomar decisiones.

### Warehouses

Son tecnologías de bases de datos que son capaces de administrar cantidades enormes de datos.

### Centros de datos

Utilizan cómputo distribuido para almacenar y procesar las grandes cantidades de datos. Un ejemplo de estas tecnologías es Hadoop.

## Retos para la tecnología

  - Manejo de datos: ¿cómo organizar todos los datos que se reciben? Se necesitan "sistemas de archivos" que manejen cantidades enormes de datos en un solo sistema.
  - Integración: se necesitan sistemas de almancenamiento que también puedan realizar el análisis de la información.
  - Análisis: se necesitan algoritmos que tengan menor complejidad que los existentes para poder realizar un buen análisis

# Data-intensive computing in the 21st Century

Los dos principales retos para las aplicaciones intensivas en datos son:

  1. Manejo y procesamiento de grandes volúmenes de datos.
  2. Reducción del tiempo de análisis de los datos.

El cómputo intensivo en datos combina la necesidad de procesar bases de datos muy grandes con complejidad computacional alta.

## Retos del cómputo intensivo en datos

Se necesitan:
  - Nuevos algoritmos con menor complejidad.
  - Nuevas tecnologías de metadatos.
  - Avances en cómputo de alto nivel.
  - Arquitecturas especializadas a resolver el problema.
  - Mejores sistemas distribuidos.
  
