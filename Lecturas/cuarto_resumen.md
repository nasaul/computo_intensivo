# Extreme Data-Intensive Scientific Computing

La disponibilidad de experimentos con una cantidad enorme de datos y el potencial de analizar la información en la computadora está cambiando la manera en que se hace la ciencia.

Sin embargo, la habilidad de realizar análisis estadísticos en bases tan grandes es limitado, a lo que le llaman *data deluge*.

## Análisis científico hoy

Existe una divergencia en velocidad de los componentes físicos de una computadora. Por un lado, el poder de procesamiento está creciendo de forma muy rápida; por el otro lado, la velocidad de los discos se ha mantenido constante y también la velocidad de transferencia de las redes.

### Cambio en la computación por los datos

El primer paso del pipeline en cómputo intensivo en datos es filtrar los datos relevantes y agregar los datos.

Las plataformas para realizar cómputo en la nube no son la solución para cómputos intensivos en datos porque maximizan los ciclos del CPU, pero no tienen una buena velocidad de memoria. Además, es muy caro el mantener y mover los datos.

## ¿Cuánto tiempo los datos seguiran creciendo?

Depende el campo de la ciencia, por ejemplo, en física se alcanzará un nivel estable de datos.

### Simulaciones numéricas

Las simulaciones numéricas se han convertido en una fuente más de generación de grandes cantidades de datos. La razón es porque antes se consideraba muy caro, en memoria, guardar una fotografía de la simulación.

### Cómputo dentro de la base de datos

Muchas de las tareas sencillas funcionan muy bien dentro de las bases de datos relacionales. Sin embargo, calculos más complejos pueden resultar más difíciles.

## Ley de Amdahl

La ley en la que se concentra el autor es: *un sistema balanceado necesita un bit de I/O por cada ciclo de CPU utilizado*. La idea es que entre más se acerque a a uno quiere decir que estamos en un mejor sistema. El problema es que no se puede llegar a uno debido al cuello de botella que son las memorias.

### Diversificación

Un mismo sistema no puede solucionar todos los problemas, por esta razón se han generado alternativas:

1. GPU's
2. Procesamiento rápido en tiempo real (no guardar los datos).
