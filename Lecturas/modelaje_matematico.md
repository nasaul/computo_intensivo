# Modelaje Matemático

> All models are wrong, but some are useful.
>
> <cite>George Box</cite>


El modelaje matemático es una actividad cognitiva en la cual se piensa en el fenómeno de interés y se intenta describir en lenguaje matemático cómo se comporta el fenómeno.

## Método científico

Primero se tiene que hacer una distinción entre dos mundos:
  1. Mundo real: es el mundo donde se observan los fenómenos y comportamientos de interés.
  2. Mundo conceptual: es la abstracción del mundo real que se tiene en la mente para intentar entender el mundo real. Para hacer el entendimiento lo más objetivo posible se sigue el método científico el cual consta de tres pasos:
    1. Observación: se realizan mediciones en el mundo real para juntar evidencia acerca del fenómeno de interés.
    2. Modelaje: se intenta simplificar la realidad para describir, explicar o predecir el fenómeno de interés.
    3. Predicción: a través de la predicción se puede comprobar qué tan cercanos a la realidad están los modelos propuestos pues se comparan con lo que ocurre en la realidad.

## Principios del modelaje matemático

Sigue los siguientes principios:

  1. Identifica lo que se necesita del modelo.
  2. Identifica las variables de interés.
  3. Identifica la información relevante que se tiene.
  4. Identifica las circunstancias en las que aplica.
  5. Identifica los principos bajo los que se gobierna el fenómeno.
  6. Identifica las relaciones y respuestas.
  7. Identifica cómo se puede validar el modelo.
  8. Identifica la forma de probar el modelo.
  9. Identifica las posibles mejoras que se pueden tener en el modelo.
  10. Identifica cómo usar el modelo.

## Algunos métodos del modelaje matemático

  1. Consistencia dimensional: las dimensiones del modelo y de la realidad deben coincidir.
  2. Abstracción: elige un nivel apropiado de detalle del problema a describir.
  3. Balance: se debe mantener los niveles de propiedades importantes del modelo.
  4. Modelos lineales: se usan modelos lineales cuando el efecto de una respuesta es directamento proporcional al impulso que los maneja.
