3.
  loop1.sh enlista los archivos dentro de la carpeta.
  loop2.sh imprime números del 1 al 10.

5. Crea 10 archivos .txt vacíos con nombres del 1 al 10.
6.
  a) awk -F";" '{print $1}' calorias.csv | sort | uniq | wc -l
      850
  b) grep "Cup" calorias.csv | wc -l
      366
  c) egrep "Apricot|White" calorias.csv > apricot_white.txt

Numpy

import numpy as np
delta = 0.1
x = np.arange(0., np.pi/2, delta)
y = np.sin(x)
(y[2:] - y[:-2]) / (2 * delta)
np.cos(x[1:-1])
