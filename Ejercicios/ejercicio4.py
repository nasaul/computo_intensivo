import numpy as np
import matplotlib.pyplot as plt

def mandelbrot(N_max, umbral, nx, ny):
    # Construye un grid c = x + 1j*y en el dominio [-2, 1] x [-1.5, 1.5]
    # como la que vimos hoy recuerdas?

    x = np.linspace(-2, 1, nx)
    y = np.linspace(-1.5, 1.5, ny)

    c = x[:, np.newaxis] + 1j*y[np.newaxis, :]
    #iteracion de Mandelbrot
    #Para cada numero complejo de tu dominio iteramos y vemos si se va a la goma
    #o pertenece al fractal.
    z = c
    for j in range(N_max):
        z = z**2 + c
    #El valor absoluto de z debe ser menor al umbral para pertenecer al conjunto
    conjunto_mandelbrot = (abs(z) < umbral)

    return conjunto_mandelbrot

conjunto_mandelbrot = mandelbrot(50, 50., 601, 401)
plt.imshow(conjunto_mandelbrot.T, extent=[-2, 1, -1.5, 1.5])
plt.gray()
plt.show()
