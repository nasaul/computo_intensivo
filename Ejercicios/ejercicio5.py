import numpy as np
import matplotlib.pyplot as plt
import scipy
from IPython import display
import time


dimension = 32
shape= 'random'
#shape= 'cross'
niter=  70

def initialize(size, shape):
    if shape == 'random':
        board = np.random.rand(size, size).round(0).astype(int)
    elif shape == 'cross':
        board = np.zeros((size, size), int)
        board[int(size/2),:] = 1
        board[:,int(size/2)] = 1
    else:
        raise NotImplementedError('Soo hay dos inicializaciones: cross y random')
    # Periodic boundary conditions
    board[0,:] = board[-1,:]
    board[:,0] = board[:,-1]
    return board

def update(board):
    # number of neighbours that each square has
    neighbours = np.zeros(board.shape)
    neighbours[1:, 1:] += board[:-1, :-1]
    neighbours[1:, :-1] += board[:-1, 1:]
    neighbours[:-1, 1:] += board[1:, :-1]
    neighbours[:-1, :-1] += board[1:, 1:]
    neighbours[:-1, :] += board[1:, :]
    neighbours[1:, :] += board[:-1, :]
    neighbours[:, :-1] += board[:, 1:]
    neighbours[:, 1:] += board[:, :-1]
    new_board = np.where(neighbours < 2, 0, board)
    new_board = np.where(neighbours > 3, 0, new_board)
    new_board = np.where(neighbours == 3, 1, new_board)
    # Periodic boundaries
    new_board[0,:] = new_board[-1,:]
    new_board[:,0] = new_board[:,-1]
    return new_board

def grafica(iter):
    plt.imshow(board, cmap = plt.cm.prism)
    plt.title('Generation {0}'.format(iter))
    plt.show()
    display.display(plt.gcf())
    display.clear_output(wait=True)


board = initialize(dimension,shape)
grafica(iter)

for iter in range(niter):
    board = update(board)
    grafica(iter)
